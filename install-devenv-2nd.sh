#! /usr/bin/env bash

function assert_not_running_as_root
{
    if [[ $USER == 'root' ]];
    then
        echo "Please do not run this script as root";
        exit
    fi
}

function cleanup_and_exit_with_status
{
    rm -rf ics-ans-devenv/
    rm -rf ics-ans-devenv-cea/

    exit $1
}

assert_not_running_as_root;

sudo yum install epel-release -y || cleanup_and_exit_with_status $?
sudo yum install git ansible -y || cleanup_and_exit_with_status $?

git clone https://bitbucket.org/europeanspallationsource/ics-ans-devenv-cea.git || cleanup_and_exit_with_status $?

ansible-playbook -i "localhost," -c local ics-ans-devenv-cea/devenv-localhost-installation-2nd.yml || cleanup_and_exit_with_status $?

echo "Finished! Please reboot your computer as soon as possible";

cleanup_and_exit_with_status 0
