#! /usr/bin/env bash

yum install epel-release -y || exit $?
yum install git ansible -y || exit $?

git clone https://bitbucket.org/europeanspallationsource/ics-ans-nfsserver-cea.git || exit $?
git clone https://bitbucket.org/europeanspallationsource/ics-ans-nfsserver.git || exit $?

ansible-playbook ics-ans-nfsserver/nfsserver.yml || exit $?
ansible-playbook ics-ans-nfsserver-cea/slave-server-localhost-installation.yml || exit $?

rm -rf ics-ans-nfsserver/
rm -rf ics-ans-nfsserver-cea/

yum update -y || exit $?
reboot

