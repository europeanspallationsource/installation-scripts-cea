#! /usr/bin/env bash

function assert_not_running_as_root
{
    if [[ $USER == 'root' ]]; then
        echo "Please do not run this script as root"
        exit
    fi
}

function cleanup_and_exit_with_status
{
    echo "Cleaning up..."
    rm -rf ics-ans-devenv/
    rm -rf ics-ans-devenv-cea/

    exit $1
}

assert_not_running_as_root;

sudo yum install epel-release -y || cleanup_and_exit_with_status $?
sudo yum install git ansible -y || cleanup_and_exit_with_status $?

git clone --branch 2.2.2 https://bitbucket.org/europeanspallationsource/ics-ans-devenv.git || cleanup_and_exit_with_status $?
git clone --branch 2.2.2 https://bitbucket.org/europeanspallationsource/ics-ans-devenv-cea.git || cleanup_and_exit_with_status $?

sudo ansible-playbook -i "localhost," -c local ics-ans-devenv/devenv.yml --extra-vars "DEVENV_SSSD=false DEVENV_EEE=mounted DEVENV_NFSSERVER=132.166.10.5 DEVENV_CSS=true DEVENV_OPENXAL=false DEVENV_IPYTHON=false" || cleanup_and_exit_with_status $?
sudo ansible-playbook -i "localhost," -c local ics-ans-devenv-cea/devenv-localhost-installation-1st.yml || cleanup_and_exit_with_status $?

echo "Finished! Please reboot your computer now";

cleanup_and_exit_with_status 0
